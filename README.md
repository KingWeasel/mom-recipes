# Mom's Recipes

## Chicken Ishtu
### Ingredients
### Instructions
## Sambar
## Biryani
## Beef Curry
## Beef/Pork Fry
## Elsy Aunty's Beef Cutlets
### Ingredients
#### Main Mix
- 1 pound beef 
    - if using sirloin, bake for 40 min with some salt/spices the day before
    - Can use ground beef if easier
    - This will be cooked with everything and then ground in a food processor
- 3-4 small golden potatoes, boiled and peeled
    - can steam in microwave, which is a little easier
- 1-2 whole green chillies diced thinly
- 1 1/2 tsp ginger garlic paste
- 1 small onion, diced
- (Optional) a few curry leaves
- (Optional) 2 tbsp vinegar
#### Spice Mix
- 1/2 tsp chili powder
- 1/4 tsp turmeric 
- 3 cardamom pods, ground
- 4 cloves, ground
- 1/2 stick cinnamon, ground
- 2 tsp fennel, ground
- 1 tsp black pepper, ground
#### Coating
- 1 cup plain bread crumbs
    - optionally, can do 1/2 cup bread crumbs and 1/2 cup panko for more crunch
 1-2 eggs, whisked
### Instructions
#### Making the Insides
1. Sautee ground beef or bake steak with salt and pepper
2. Blend green chillies, onion, and curry leaves in a food processor
3. Sautee this mix for a little, then add ![Spice Mix](#spice-mix). Continue sauteeing for a minute
4. Blend ground beef in processor
5. Mix beef and ingredients from step 3 very well
6. Mash the potatoes and mix it with everything
7. (Optional) add vinegar
#### Getting Ready for Frying
1. Pour bread crumbs/panko in a bowl
2. Put whisked eggs in a separate bowl
3. Compact and form the beef/potato mix into small ovals (should fit in your hand comfortably)
4. For each cutlet:
    - With one hand, dip the cutlet in the whisked eggs and cover the entire surface
    - With the same hand, pick up the cutlet, shake off excess egg, then drop the cutlet in the bowl of coating
    - With the other hand, roll the cutlet around the coating until the entire surface is coated
    - With that same hand, put cutlet aside
#### Frying
1. Fry the cutlets.



